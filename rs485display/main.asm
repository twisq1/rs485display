////////////////////////////////////////////////////////////////////////////////////////////////////////////
// RS485 DISPLAY
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RS485 DISPLAY is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RS485 DISPLAY.  If not, see <http://www.gnu.org/licenses/>.
//
// Version 1.0 / 2018-01-24
//
// TWISQ (http://www.twisq.nl)
////////////////////////////////////////////////////////////////////////////////////////////////////////////


		// Configuration
		.equ	TEST_INTERVAL	= 0		; If TEST != 0, this is the test probe which sends testdata

		// Register definitions
		.def	LOOKUP		= r0	; result of LPM
		.def	LED_TIMER	= r1	; When reached zero, turn LED off
		.def	RX_STATE    = r2
		.def	TEST_STATE	= r3
		.def	TEST_TIMER	= r4	; When reached zero, send test character
		.def	TEST_CHARACTER = r5	; Character to send
		.def	MY_ADDRESS	= r6    ; Address read by DIP switches

		.def	A			= r16
		.def	B			= r17
		.def	C			= r18

		// Port D bit definitions
		.equ	RS485_SEND	= 2
		.equ	SR_CLK		= 3
		.equ	SR_LOAD		= 4
		.equ	SR_DATA		= 5
		.equ	LED			= 6

		.equ	RX_BUFFER	= 0x60
		.equ	RX_END		= 0x70

		.equ	RXSTAT_WAIT		= 1
		.equ	RXSTAT_READY	= 2
		.equ	START_OF_FRAME	= '['
		.equ	END_OF_FRAME	= ']'

////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// Interupt table
		rjmp	INT_RESET			; 1 - Reset Handle
		rjmp	INT_RESET			; 2 - External interrupt 0
		rjmp	INT_RESET			; 3 - External interrupt 1
		rjmp	INT_RESET			; 4 - Timer 1 Capture
		rjmp	INT_RESET			; 5 - Timer 1 Compare
		rjmp	INT_RESET			; 6 - Timer 1 Overflow
		rjmp	INT_T0_OVERFLOW		; 7 - Timer 0 Overflow
		rjmp	INT_UART_RX			; 8 - UART Receive complete
		rjmp	INT_RESET			; 9 - UART Data register empty
		rjmp	INT_RESET			;10 - UART Transmit complete
		rjmp	INT_RESET			;11 - Analog comparator

////////////////////////////////////////////////////////////////////////////////////////////////////////////

ASCII_TAB:
		.db	0xf9, 0x60				; '0', '1'
		.db 0xda, 0xf2				; '2', '3'
		.db 0x63, 0xb3				; '4', '5'
		.db 0xbb, 0xe0				; '6', '7'
		.db 0xfb, 0xf3				; '8', '9'
    	.db 0xeb, 0x3b				; 'A', 'b'
		.db 0x99, 0x7a				; 'C', 'd'
    	.db 0x9b, 0x8b				; 'E', 'F'
		.db 0x00, 0x80				;  , a
		.db 0x40, 0x20				; b, c
		.db 0x10, 0x08				; d, e
		.db	0x01, 0x02				; f, g
		.db 0xfb, 0x7b
		.db 0xbb, 0xdb
		.db 0xeb, 0xf3
		.db	0xfa, 0xf9

		.db	0x00, 0x60				; ' ', '!'
		.db 0x41, 0x7b				; '"', '#'
		.db 0xb3, 0x4a				; '$', '%'
		.db 0x3b, 0x40				; '&', '''
		.db 0x99, 0xf0				; '(', ')'
    	.db	0x62, 0x62				; '*', '+'
    	.db	0x10, 0x02				; ',', '-'
    	.db 0x10, 0x4a				; '.', '/'
		.db	0xf9, 0x60				; '0', '1'
		.db 0xda, 0xf2				; '2', '3'
		.db 0x63, 0xb3				; '4', '5'
		.db 0xbb, 0xe0				; '6', '7'
		.db 0xfb, 0xf3				; '8', '9'
		.db 0x90, 0x90				; ':', ';'
    	.db 0x1a, 0x12				; '<', '='
    	.db 0x32, 0xca				; '>', '?'
    	.db 0xfa, 0xeb				; '@', 'A'
		.db 0xfb, 0x99				; 'B', 'C'
    	.db 0xf9, 0x9b				; 'D', 'E'
    	.db 0x8b, 0xb9				; 'F', 'G'
		.db 0x6b, 0x60				; 'H', 'I'
		.db 0x70, 0x6b				; 'J', 'K'
    	.db 0x19, 0xe9				; 'L', 'M'
	    .db 0xe9, 0xf9				; 'N', 'O'
		.db 0xcb, 0xf9				; 'P', 'Q'
		.db 0xeb, 0xb3				; 'R', 'S'
		.db 0xe0, 0x79				; 'T', 'U'
		.db 0x79, 0x79				; 'V', 'W'
    	.db 0x6b, 0x63				; 'X', 'Y'
    	.db 0xda, 0x99				; 'Z', '['
    	.db 0x23, 0xf0				; '\', ']'
    	.db 0x80, 0x10				; '^', '_'
    	.db 0x01, 0xfa				; '`', 'a'
    	.db 0x3b, 0x1a				; 'b', 'c'
    	.db 0x7a, 0x9b				; 'd', 'e'
    	.db	0x8b, 0xf3				; 'f', 'g'
    	.db 0x2b, 0x20				; 'h', 'i'
    	.db 0x30, 0x0b				; 'j', 'k'
    	.db 0x60, 0x2a				; 'l', 'm'
    	.db 0x2a, 0x3a				; 'n', 'o'
    	.db 0xcb, 0xe3				; 'p', 'q'
    	.db 0x0a, 0xb3				; 'r', 's'
    	.db 0x1b, 0x38				; 't', 'u'
    	.db 0x38, 0x38				; 'v', 'w'
    	.db 0x28, 0x63				; 'x', 'y'
    	.db 0xda, 0x1a				; 'z', '{'
    	.db 0x09, 0x32				; '|', '}'
    	.db 0xc3, 0x00				; '~', ' '

////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
		// Start of program
INT_RESET:
		nop
		ldi		A, low(RAMEND)		; init stack pointer
		out		SPL, A
		ldi		A, 0x5E
		out		DDRD, A				; port D, bits 1, 2, 3, 4, 6 are outputs
		clr		A
		out		PORTD, A			; all bits low
		ser		A
		out		DDRB, A				; port B, all bits are outputs

		ldi		A, 1
		out		UBRR, A				; 115200 Baud at 3,6864 MHz
		ldi		A, 0x98
		out		UCSRB, A			; enable receiver + transmitter + interrupts

		ldi		A, 0x05
		out		TCCR0B, A			; Timer/counter0 CK/1024 = 3600 Hz
		ldi		A, -72
		out		TCNT0, A			; Load counter
		ldi		A, 0x02
		out		TIMSK, A			; Enable T0 overflow interrupt

		.if		TEST_INTERVAL != 0
		ldi		A, TEST_INTERVAL	; If testing, init test timer
		mov		TEST_TIMER, A
		clr		A
		mov		TEST_STATE, A		; Init test state to zero
		ldi		A, '0'
		mov		TEST_CHARACTER, A
		.endif

		ldi		YH, RXSTAT_WAIT		; Wait for start of frame
		sei

		ldi		A, 50
		rcall	BLINK_LED			; turn LED on for 1 second

INFINITY:
		cpi		YH, RXSTAT_READY
		brne	INFINITY
		ldi		YH, RXSTAT_WAIT

		ldi		A, 4
		rcall	BLINK_LED

		ldi		XL, RX_BUFFER
		clr		XH
		rcall	READ_HEX_BYTE
		brne	INFINITY			; If Z=0, there has been an invalid digit in the address
		mov		B, A
		rcall	READ_ADDRESS
		mov		MY_ADDRESS, A		; Store my address. It is used in test mode.
		cp		A, B
		brne	INFINITY

		ld		A, X+
		ld		B, X				; Test if DP must be on
		cpi		B, '.'
		brne	NO_DP
		sbr		A, 1<<7				; If so, set bit 7 in 'ASCII' code
NO_DP:
		rcall	CONVERT_ASCII
		out		PORTB, A

		ldi		A, 20
		rcall	BLINK_LED

		rjmp	INFINITY

////////////////////////////////////////////////////////////////////////////////////////////////////////////

BLINK_LED:
		cbi		PORTD, LED			; Turn LED on
		mov		LED_TIMER, A		; Load counter
		ret

////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// Timer 0 interrupt
INT_T0_OVERFLOW:
		push	A
		in		A, SREG
		push	A					; Save status register
		ldi		A, -72				; This counter makes it a 50Hz interrupt
		out		TCNT0, A			; Reset counter

		.if		TEST_INTERVAL != 0
		tst		TEST_TIMER			; test if we are in test mode
		breq	IT0_NO_TEST
		dec		TEST_TIMER
		brne	IT0_NO_TEST
		rcall	SEND_TEST_CHARACTER
		ldi		A, TEST_INTERVAL
		mov		TEST_TIMER, A
		.endif

IT0_NO_TEST:
		tst		LED_TIMER			; test if we need to check the LED
		breq	IT0_END
		dec		LED_TIMER			; count down and shut down at zero
		brne	IT0_END
		sbi		PORTD, LED			; Shut down LED
IT0_END:
		pop		A					; Restore status register
		out		SREG, A
		pop		A
		reti

////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// UART Receive interrupt
INT_UART_RX:
		push	A					; save ACCU
		in		A, SREG
		push	A					; save status register

		in		A, UDR
		rcall	PROCESS_RX

		pop		A
		out		SREG, A				; restore status register
		pop		A					; restore ACCU
		reti

PROCESS_RX:
		cpi		A, START_OF_FRAME
		brne	IU_NO_START
		ldi		YL, RX_BUFFER
		clr		YH
		rjmp	IU_EXIT

IU_NO_START:
		tst		YH
		brne	IU_EXIT
		ST		Y+, A				; store character
		cpi		A, END_OF_FRAME
		brne	IU_NO_END
		ldi		YH, RXSTAT_READY	; sign receiver ready
		rjmp	IU_EXIT

IU_NO_END:
		cpi		YL, RX_END
		brcs	IU_EXIT
		ldi		YH, RXSTAT_WAIT
IU_EXIT:
		ret

////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// Read address from DIP switches.
		// Return value in A
READ_ADDRESS:
		cbi		PORTD, SR_LOAD		; load switches
		nop
		sbi		PORTD, SR_LOAD
		ldi		C, 8				; loop 8 times
RA_LOOP:
		clc							; assume bit=0
		sbic	PIND, SR_DATA		; test if assumption is true
		sec							; if not, set bit
		rol		A					; shift bit into register
		cbi		PORTD, SR_CLK		; pull clock down
		nop
		sbi		PORTD, SR_CLK		; clock bit in
		dec		C
		brne	RA_LOOP				; do next bit
		com		A					; invert all bits
		ret

////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// Convert ASCII code in A to 7-seg code in A
CONVERT_ASCII:
		ldi		B, 0x04				; decimal point
		sbrs	A, 7				; test if DP must be set
		clr		B
		andi	A, 0x7F				; get rid of DP bit
		ldi		ZL,low(ASCII_TAB*2)	; get conversion table
		ldi		ZH,high(ASCII_TAB*2)
		add		ZL,A				; add character
		lpm							; look up 7-segment code
		mov		A, LOOKUP
		or		A, B				; add decimal point if needed
		ret

////////////////////////////////////////////////////////////////////////////////////////////////////////////

READ_HEX_BYTE:
		ld		A, X+				; get first nibble
		rcall	CONVERT_HEX_NIBBLE
		brne	RHB_ERROR			; if Z=0, error because of invalid character
		swap	A					; * 16
		mov		B, A
		ld		A, X+				; get next nibble
		rcall	CONVERT_HEX_NIBBLE
		brne	RHB_ERROR			; if Z=0, error because of invalid character
		add		A, B				; add nibbles
		sez							; set zero flag to indicate "no error"
RHB_ERROR:
		ret

CONVERT_HEX_NIBBLE:
		cpi		A, 'a'				; test if it is lower case
		brcs	CHN_CASEOK
		subi	A, 32
CHN_CASEOK:
		cpi		A, 'A'				; test if it is alphanumeric
		brcs	CHN_OK
		subi	A, 7
CHN_OK:
		subi	A, '0'
		mov		C, A
		andi	C, 0xF0				; normally all upper four bits should be zero
		ret

////////////////////////////////////////////////////////////////////////////////////////////////////////////

SEND_TEST_CHARACTER:
		mov		C, TEST_STATE
		inc		C
		mov		TEST_STATE, C
		dec		C
		brne	STC_N1
		ldi		A, START_OF_FRAME
		rjmp	STC_END
STC_N1:
		dec		C
		brne	STC_N2
		mov		A, MY_ADDRESS
		swap	A
		andi	A, 0x0F
		rcall	TO_HEX_CHAR
		rjmp	STC_END
STC_N2:
		dec		C
		brne	STC_N3
		mov		A, MY_ADDRESS
		andi	A, 0x0F
		rcall	TO_HEX_CHAR
		rjmp	STC_END
STC_N3:
		dec		C
		brne	STC_N4
		mov		A, TEST_CHARACTER
		rjmp	STC_END
STC_N4:
		clr		C
		mov		TEST_STATE, C
		mov		A, TEST_CHARACTER
		inc		A
		cpi		A, '9'+1
		brcs	STC_NO
		ldi		A, '0'
STC_NO:
		mov		TEST_CHARACTER, A
		ldi		A, END_OF_FRAME
STC_END:
		sbi		PORTD, RS485_SEND	; Enable TX output
		out		UDR, A				; Send character
		rcall	PROCESS_RX
		ret

TO_HEX_CHAR:
		subi	A, -'0'
		cpi		A, '9'+1
		brcs	THC_OK
		subi	A, -7
THC_OK:
		ret