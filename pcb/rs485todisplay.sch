{
    "Application": "QScheme",
    "Manufacturer": "TwisQ",
    "Url": "https://www.twisq.nl/qscheme",
    "Version": "0.2.0",
    "library": [
        {
            "ordercode": "1105982",
            "pins": [
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "S/L",
                    "number": 1,
                    "position": "R4"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "CLK",
                    "number": 2,
                    "position": "R2"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "I4",
                    "number": 3,
                    "position": "L4"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "I5",
                    "number": 4,
                    "position": "L3"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "I6",
                    "number": 5,
                    "position": "L2"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "I7",
                    "number": 6,
                    "position": "L1"
                },
                {
                    "electype": 2,
                    "linetype": 1,
                    "name": "NOUT",
                    "number": 7,
                    "position": "R7"
                },
                {
                    "electype": 4,
                    "linetype": 1,
                    "name": "GND",
                    "number": 8,
                    "position": "R8"
                },
                {
                    "electype": 2,
                    "linetype": 1,
                    "name": "OUT",
                    "number": 9,
                    "position": "R6"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "SER",
                    "number": 10,
                    "position": "R5"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "I0",
                    "number": 11,
                    "position": "L8"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "I1",
                    "number": 12,
                    "position": "L7"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "I2",
                    "number": 13,
                    "position": "L6"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "I3",
                    "number": 14,
                    "position": "L5"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "INH",
                    "number": 15,
                    "position": "R3"
                },
                {
                    "electype": 4,
                    "linetype": 1,
                    "name": "VCC",
                    "number": 16,
                    "position": "R1"
                }
            ],
            "price": 0.69,
            "reference": "Q4",
            "shape": "DIL-16",
            "sizex": 8,
            "sizey": 9,
            "symbol": "74165.cmp",
            "value": "74HCT165"
        },
        {
            "ordercode": "1102956",
            "pins": [
                {
                    "electype": 2,
                    "linetype": 1,
                    "name": "R",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "RE",
                    "number": 2,
                    "position": "L2"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "DE",
                    "number": 3,
                    "position": "L3"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "D",
                    "number": 4,
                    "position": "L4"
                },
                {
                    "electype": 4,
                    "linetype": 1,
                    "name": "GND",
                    "number": 5,
                    "position": "R4"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "A",
                    "number": 6,
                    "position": "R3"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "B",
                    "number": 7,
                    "position": "R2"
                },
                {
                    "electype": 4,
                    "linetype": 1,
                    "name": "VCC",
                    "number": 8,
                    "position": "R1"
                }
            ],
            "price": 4.23,
            "reference": "Q3",
            "shape": "DIL-08",
            "sizex": 6,
            "sizey": 5,
            "symbol": "75lbc184.cmp",
            "value": "75LBC184"
        },
        {
            "ordercode": "9171592",
            "pins": [
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "RESET",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "RX",
                    "number": 2,
                    "position": "L2"
                },
                {
                    "electype": 2,
                    "linetype": 1,
                    "name": "TX",
                    "number": 3,
                    "position": "L3"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "XTAL2",
                    "number": 4,
                    "position": "L4"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "XTAL1",
                    "number": 5,
                    "position": "L5"
                },
                {
                    "electype": 3,
                    "linetype": 1,
                    "name": "PD2",
                    "number": 6,
                    "position": "L6"
                },
                {
                    "electype": 3,
                    "linetype": 1,
                    "name": "PD3",
                    "number": 7,
                    "position": "L7"
                },
                {
                    "electype": 3,
                    "linetype": 1,
                    "name": "PD4",
                    "number": 8,
                    "position": "L8"
                },
                {
                    "electype": 3,
                    "linetype": 1,
                    "name": "PD5",
                    "number": 9,
                    "position": "L9"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "GND",
                    "number": 10,
                    "position": "L10"
                },
                {
                    "electype": 3,
                    "linetype": 1,
                    "name": "PD6",
                    "number": 11,
                    "position": "R10"
                },
                {
                    "electype": 3,
                    "linetype": 1,
                    "name": "PB0",
                    "number": 12,
                    "position": "R9"
                },
                {
                    "electype": 3,
                    "linetype": 1,
                    "name": "PB1",
                    "number": 13,
                    "position": "R8"
                },
                {
                    "electype": 3,
                    "linetype": 1,
                    "name": "PB2",
                    "number": 14,
                    "position": "R7"
                },
                {
                    "electype": 3,
                    "linetype": 1,
                    "name": "PB3",
                    "number": 15,
                    "position": "R6"
                },
                {
                    "electype": 3,
                    "linetype": 1,
                    "name": "PB4",
                    "number": 16,
                    "position": "R5"
                },
                {
                    "electype": 3,
                    "linetype": 1,
                    "name": "PB5",
                    "number": 17,
                    "position": "R4"
                },
                {
                    "electype": 3,
                    "linetype": 1,
                    "name": "PB6",
                    "number": 18,
                    "position": "R3"
                },
                {
                    "electype": 3,
                    "linetype": 1,
                    "name": "PB7",
                    "number": 19,
                    "position": "R2"
                },
                {
                    "electype": 4,
                    "linetype": 1,
                    "name": "VCC",
                    "number": 20,
                    "position": "R1"
                }
            ],
            "price": 1.39,
            "reference": "Q?",
            "shape": "DIP-20",
            "sizex": 8,
            "sizey": 11,
            "symbol": "attiny2313.cmp",
            "value": "ATtiny2313"
        },
        {
            "ordercode": "1141777",
            "pins": [
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 1,
                    "position": "T1"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 2,
                    "position": "B1"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAABUAAAALCAIAAAAfqVEqAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAOElEQVQokWP8//8/Ax7AyMiAVwETPs1EABaoJfidgBX8/w/Tj98LeAGl7qeD/3EBuvifkNMGOvwAlqkNG4oGlW8AAAAASUVORK5CYII=",
            "price": 0.08,
            "reference": "C?",
            "shape": "C-02",
            "sizex": 2,
            "sizey": 1,
            "symbol": "capacitor.cmp",
            "value": "100n"
        },
        {
            "ordercode": "",
            "pins": [
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 2,
                    "position": "R1"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAABUAAAAVCAIAAAAmdTLBAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAXUlEQVQ4jWP8//8/AwWABV2AkRGnWmw2YejHoQ6XuUw4bSMOwPTjcTZWAFNPLftHqn4WkkMeDhgZGRgYWKCphQxT/v9nGHj/Dxf9pJYiMPXY8j8pcYmhn0SHUOp/ADGaETXCCoZFAAAAAElFTkSuQmCC",
            "price": 0,
            "reference": "X?",
            "shape": "HC18U-1",
            "sizex": 2,
            "sizey": 2,
            "symbol": "crystal.cmp",
            "value": "1MHz"
        },
        {
            "ordercode": "",
            "pins": [
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "CATHODE",
                    "number": 2,
                    "position": "R1"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "ANODE",
                    "number": 1,
                    "position": "L1"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAAB8AAAAVCAIAAAAxV6IIAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAfklEQVQ4jcWVUQoAIQhEm+j+V54+giViqRlt2X6l90RRQbJ89uohDsSjZ7qAyNETAo1OxgQaPSqQKzPQpkCuzGy6TF80ssCkA5YA0qSOlj6NlTvcyn4TvFJmx/Z7U1JYTfJqMuvuoGW6n7VDD6EdeugM+LN6mZ44Xvj18uVeBwt2KytjaUjNAAAAAElFTkSuQmCC",
            "price": 0.1,
            "reference": "D?",
            "shape": "DIODE-04",
            "sizex": 3,
            "sizey": 2,
            "symbol": "diode.sbl",
            "value": "1N4148"
        },
        {
            "ordercode": "",
            "pins": [
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "A1",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "A2",
                    "number": 2,
                    "position": "L2"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "A3",
                    "number": 3,
                    "position": "L3"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "A4",
                    "number": 4,
                    "position": "L4"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "A5",
                    "number": 5,
                    "position": "L5"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "A6",
                    "number": 6,
                    "position": "L6"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "A7",
                    "number": 7,
                    "position": "L7"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "A8",
                    "number": 8,
                    "position": "L8"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "B1",
                    "number": 16,
                    "position": "R1"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "B2",
                    "number": 15,
                    "position": "R2"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "B3",
                    "number": 14,
                    "position": "R3"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "B4",
                    "number": 13,
                    "position": "R4"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "B5",
                    "number": 12,
                    "position": "R5"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "B6",
                    "number": 11,
                    "position": "R6"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "B7",
                    "number": 10,
                    "position": "R7"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "B8",
                    "number": 9,
                    "position": "R8"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAACkAAABbCAIAAABh6DtrAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAA/klEQVRoge2aQQ6FIAxEW+P9r8xfuCD5oYAdCkRmtmgfUHgLoyZZlltEJK2YgOo1gWGNxLPt3ANq2CuLZ0v1xMTuef2o2qO79dvbPzPG0uF+N93wut89toGNZKy7Z9ufZ4AGldizFLvynJN9Fntbr/U4C/CawU5JVBsbAGsA6DfcGsCp8Azo1LPYe7rlueLeujlOtwB1e1J1Kr0WMgN67Sz2nl7D6uY4vdYsHeg1uLqXHX/LYa8NZtNr32Vv67Uhh87jtepr/0+6Yvc7/orTazn02nfZ9FqBSq/NZNNrZA8PvVag0msz2fQa2cNDrxWo53mN3+7JJptsKLrwv9wfV6dpF7kgoBwAAAAASUVORK5CYII=",
            "price": 0,
            "reference": "S?",
            "shape": "DIP-16",
            "sizex": 4,
            "sizey": 9,
            "symbol": "dipswitch8.cmp",
            "value": ""
        },
        {
            "ordercode": "",
            "pins": [
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "A1",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "A2",
                    "number": 2,
                    "position": "L2"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "A3",
                    "number": 3,
                    "position": "L3"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "A4",
                    "number": 4,
                    "position": "L4"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "A5",
                    "number": 5,
                    "position": "L5"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "A6",
                    "number": 6,
                    "position": "L6"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "A7",
                    "number": 7,
                    "position": "L7"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "A8",
                    "number": 8,
                    "position": "L8"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "B1",
                    "number": 16,
                    "position": "R1"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "B2",
                    "number": 15,
                    "position": "R2"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "B3",
                    "number": 14,
                    "position": "R3"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "B4",
                    "number": 13,
                    "position": "R4"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "B5",
                    "number": 12,
                    "position": "R5"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "B6",
                    "number": 11,
                    "position": "R6"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "B7",
                    "number": 10,
                    "position": "R7"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "B8",
                    "number": 9,
                    "position": "R8"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAACkAAABbCAIAAABh6DtrAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAA/klEQVRoge2aQQ6FIAxEW+P9r8xfuCD5oYAdCkRmtmgfUHgLoyZZlltEJK2YgOo1gWGNxLPt3ANq2CuLZ0v1xMTuef2o2qO79dvbPzPG0uF+N93wut89toGNZKy7Z9ufZ4AGldizFLvynJN9Fntbr/U4C/CawU5JVBsbAGsA6DfcGsCp8Azo1LPYe7rlueLeujlOtwB1e1J1Kr0WMgN67Sz2nl7D6uY4vdYsHeg1uLqXHX/LYa8NZtNr32Vv67Uhh87jtepr/0+6Yvc7/orTazn02nfZ9FqBSq/NZNNrZA8PvVag0msz2fQa2cNDrxWo53mN3+7JJptsKLrwv9wfV6dpF7kgoBwAAAAASUVORK5CYII=",
            "price": 0,
            "reference": "S?",
            "shape": "DIL-16",
            "sizex": 4,
            "sizey": 9,
            "symbol": "dipswitch8.sbl",
            "value": ""
        },
        {
            "ordercode": "",
            "pins": [
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 2,
                    "position": "R1"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 3,
                    "position": "L2"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 4,
                    "position": "R2"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 5,
                    "position": "L3"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 6,
                    "position": "R3"
                }
            ],
            "price": 0,
            "reference": "K?",
            "shape": "SIL-2X03",
            "sizex": 2,
            "sizey": 4,
            "symbol": "header2x3.sbl",
            "value": ""
        },
        {
            "ordercode": "",
            "pins": [
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 2,
                    "position": "R1"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 3,
                    "position": "L2"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 4,
                    "position": "R2"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 5,
                    "position": "L3"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 6,
                    "position": "R3"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 7,
                    "position": "L4"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 8,
                    "position": "R4"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 9,
                    "position": "L5"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 10,
                    "position": "R5"
                }
            ],
            "price": 0,
            "reference": "K?",
            "shape": "SIL-2X05",
            "sizex": 2,
            "sizey": 6,
            "symbol": "header2x5.cmp",
            "value": ""
        },
        {
            "ordercode": "",
            "pins": [
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 2,
                    "position": "L2"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 3,
                    "position": "L3"
                }
            ],
            "price": 0,
            "reference": "K?",
            "shape": "SCREW-03",
            "sizex": 2,
            "sizey": 4,
            "symbol": "header3.cmp",
            "value": ""
        },
        {
            "ordercode": "",
            "pins": [
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "CATHODE",
                    "number": 2,
                    "position": "R1"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "ANODE",
                    "number": 1,
                    "position": "L1"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAACkAAAAVCAIAAABUulB3AAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAtElEQVRIicWWURKAIAhEpfH+V6YPZhhK0DVw2r9AfSxqRczc8iJ6PNo1XymTvdYLIVlZzrURe/PYAphX4DKIQpIXD9gydAsv4HGKFjTgA7adiYNHwKsTz+yKjeAjgLsFJgKwt8DLuBHGjqwnwDDbPURpwb5d/M6NGtUhQ3p3ZXBRD7pT42R3tZQKAT1Xu8xVVJit4GrB5/yA+i9U0dT3SXDMxl5MZ9jnwa01qvln+qT0dyyhG78lWBvGcBRTAAAAAElFTkSuQmCC",
            "price": 0,
            "reference": "D?",
            "shape": "LED-3MM",
            "sizex": 4,
            "sizey": 2,
            "symbol": "led.sbl",
            "value": "LED"
        },
        {
            "ordercode": "9756078",
            "pins": [
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "IN",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "GND",
                    "number": 2,
                    "position": "B3"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "OUT",
                    "number": 3,
                    "position": "R1"
                }
            ],
            "price": 0.47,
            "reference": "Q?",
            "shape": "TO-220",
            "sizex": 6,
            "sizey": 5,
            "symbol": "regulator.cmp",
            "value": "7805"
        },
        {
            "ordercode": "9339051",
            "pins": [
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 1,
                    "position": "T1"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 2,
                    "position": "B1"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAABUAAAAfCAIAAACH7hGnAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAOklEQVQ4jWP8//8/AxpgZEQXgQBMlQwMLMQqxWEoE3b9RINR/aP6R/WP6h/VP6p/KOrHUX/iqoIxAAAlzQdBlnBBCAAAAABJRU5ErkJggg==",
            "price": 0.05,
            "reference": "R1",
            "shape": "R-04",
            "sizex": 2,
            "sizey": 3,
            "symbol": "resistor.cmp",
            "value": "120"
        },
        {
            "ordercode": "9339051",
            "pins": [
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 1,
                    "position": "T1"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 2,
                    "position": "B1"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAABUAAAAfCAIAAACH7hGnAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAOklEQVQ4jWP8//8/AxpgZEQXgQBMlQwMLMQqxWEoE3b9RINR/aP6R/WP6h/VP6p/KOrHUX/iqoIxAAAlzQdBlnBBCAAAAABJRU5ErkJggg==",
            "price": 0.05,
            "reference": "R?",
            "shape": "R-04",
            "sizex": 2,
            "sizey": 3,
            "symbol": "resistor.sbl",
            "value": "1k"
        },
        {
            "ordercode": "",
            "pins": [
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "COMMON",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "R1",
                    "number": 2,
                    "position": "L2"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "R2",
                    "number": 3,
                    "position": "L3"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "R3",
                    "number": 4,
                    "position": "L4"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "R4",
                    "number": 5,
                    "position": "L5"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "R5",
                    "number": 6,
                    "position": "L6"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "R6",
                    "number": 7,
                    "position": "L7"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "R7",
                    "number": 8,
                    "position": "L8"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "R8",
                    "number": 9,
                    "position": "L9"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAADMAAABlCAIAAABvFEyXAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAA4klEQVRoge2aQQ4CIRTFxHD/K+PKRETj1DjwMO1ONnbm8+3G0lq7RHJdLfAWzTiacTTjaMYpKQUYUlTHowWUMp7lTlMzTq5Z7T69uom/ge9ZfT44Y1W/euDcaeaa1RPv1iP8W/oGrNqAvRow7Oac4R6gN0uo+51dpmkDjpA7zVwzG2ADZmADODaAkzvNXDMb8E8NyDXLrdNG78xf2o9oxrEBNmAGNoBjAziacWyADZiBDeDYAI5mHBtgA2ZgAzg2gKMZxwbYgBnYAI4N4GjGsQF8sXKnmWuW2wD/scTRjKMZ5waffkU5h+2X0gAAAABJRU5ErkJggg==",
            "price": 0,
            "reference": "R?",
            "shape": "SIL-1X09",
            "sizex": 5,
            "sizey": 10,
            "symbol": "rnet.sbl",
            "value": "8x10k"
        },
        {
            "ordercode": "1094428",
            "pins": [
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "I0",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "I1",
                    "number": 2,
                    "position": "L2"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "I2",
                    "number": 3,
                    "position": "L3"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "I3",
                    "number": 4,
                    "position": "L4"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "I4",
                    "number": 5,
                    "position": "L5"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "I5",
                    "number": 6,
                    "position": "L6"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "I6",
                    "number": 7,
                    "position": "L7"
                },
                {
                    "electype": 1,
                    "linetype": 1,
                    "name": "I7",
                    "number": 8,
                    "position": "L8"
                },
                {
                    "electype": 4,
                    "linetype": 1,
                    "name": "GND",
                    "number": 9,
                    "position": "L9"
                },
                {
                    "electype": 0,
                    "linetype": 1,
                    "name": "V+",
                    "number": 10,
                    "position": "R9"
                },
                {
                    "electype": 2,
                    "linetype": 1,
                    "name": "O7",
                    "number": 11,
                    "position": "R8"
                },
                {
                    "electype": 2,
                    "linetype": 1,
                    "name": "O6",
                    "number": 12,
                    "position": "R7"
                },
                {
                    "electype": 2,
                    "linetype": 1,
                    "name": "O5",
                    "number": 13,
                    "position": "R6"
                },
                {
                    "electype": 2,
                    "linetype": 1,
                    "name": "O4",
                    "number": 14,
                    "position": "R5"
                },
                {
                    "electype": 2,
                    "linetype": 1,
                    "name": "O3",
                    "number": 15,
                    "position": "R4"
                },
                {
                    "electype": 2,
                    "linetype": 1,
                    "name": "O2",
                    "number": 16,
                    "position": "R3"
                },
                {
                    "electype": 2,
                    "linetype": 1,
                    "name": "O1",
                    "number": 17,
                    "position": "R2"
                },
                {
                    "electype": 2,
                    "linetype": 1,
                    "name": "O0",
                    "number": 18,
                    "position": "R1"
                }
            ],
            "price": 0.39,
            "reference": "Q2",
            "shape": "DIL-18",
            "sizex": 6,
            "sizey": 10,
            "symbol": "uln2803.cmp",
            "value": "ULN2803"
        }
    ],
    "sheet": {
        "component": [
            {
                "angle": 0,
                "mirrored": false,
                "ordercode": "7992009",
                "posx": 82,
                "posy": 32,
                "price": 0.961,
                "reference": "K1",
                "referenceDisplacement": {
                    "x": 0,
                    "y": -1
                },
                "shape": "SIL-2X05",
                "symbol": "header2x5.cmp",
                "value": "HEADER",
                "valueDisplacement": {
                    "x": 0,
                    "y": 6
                }
            },
            {
                "angle": 0,
                "mirrored": false,
                "ordercode": "1094428",
                "posx": 64,
                "posy": 32,
                "price": 0.397,
                "reference": "Q2",
                "referenceDisplacement": {
                    "x": 0,
                    "y": -1
                },
                "shape": "DIL-18",
                "symbol": "uln2803.cmp",
                "value": "ULN2803",
                "valueDisplacement": {
                    "x": 0,
                    "y": 10
                }
            },
            {
                "angle": 0,
                "mirrored": false,
                "ordercode": "1841616",
                "posx": 47,
                "posy": 31,
                "price": 1.15,
                "reference": "Q1",
                "referenceDisplacement": {
                    "x": 0,
                    "y": -1
                },
                "shape": "DIL-20",
                "symbol": "attiny2313.cmp",
                "value": "ATtiny2313",
                "valueDisplacement": {
                    "x": 0,
                    "y": 11
                }
            },
            {
                "angle": 0,
                "mirrored": false,
                "ordercode": "1611855",
                "posx": 32,
                "posy": 35,
                "price": 0.261,
                "reference": "X1",
                "referenceDisplacement": {
                    "x": 0,
                    "y": -1
                },
                "shape": "HC18U-1",
                "symbol": "crystal.cmp",
                "value": "3.6864MHz",
                "valueDisplacement": {
                    "x": 0,
                    "y": 2
                }
            },
            {
                "angle": 0,
                "mirrored": false,
                "ordercode": "9411674",
                "posx": 29,
                "posy": 39,
                "price": 0.0845,
                "reference": "C1",
                "referenceDisplacement": {
                    "x": 2.5,
                    "y": 0
                },
                "shape": "C-01",
                "symbol": "capacitor.cmp",
                "value": "22p",
                "valueDisplacement": {
                    "x": 2.5,
                    "y": 1
                }
            },
            {
                "angle": 0,
                "mirrored": false,
                "ordercode": "9411674",
                "posx": 35,
                "posy": 39,
                "price": 0.0845,
                "reference": "C2",
                "referenceDisplacement": {
                    "x": 2.5,
                    "y": 0
                },
                "shape": "C-01",
                "symbol": "capacitor.cmp",
                "value": "22p",
                "valueDisplacement": {
                    "x": 2.5,
                    "y": 1
                }
            },
            {
                "angle": 0,
                "mirrored": true,
                "ordercode": "1102956",
                "posx": 24,
                "posy": 19,
                "price": 4.23,
                "reference": "Q3",
                "referenceDisplacement": {
                    "x": 0,
                    "y": -1
                },
                "shape": "DIL-08",
                "symbol": "75lbc184.cmp",
                "value": "SN75LBC184P",
                "valueDisplacement": {
                    "x": 0,
                    "y": 5
                }
            },
            {
                "angle": 0,
                "mirrored": true,
                "ordercode": "3705274",
                "posx": 7,
                "posy": 20,
                "price": 0.761,
                "reference": "K3",
                "referenceDisplacement": {
                    "x": 0,
                    "y": -1
                },
                "shape": "SCREW-03",
                "symbol": "header3.cmp",
                "value": "RS485",
                "valueDisplacement": {
                    "x": 0,
                    "y": 4
                }
            },
            {
                "angle": 0,
                "mirrored": true,
                "ordercode": "3705274",
                "posx": 7,
                "posy": 11,
                "price": 0.761,
                "reference": "K4",
                "referenceDisplacement": {
                    "x": 0,
                    "y": -1
                },
                "shape": "SCREW-03",
                "symbol": "header3.cmp",
                "value": "RS485",
                "valueDisplacement": {
                    "x": 0,
                    "y": 4
                }
            },
            {
                "angle": 0,
                "mirrored": false,
                "ordercode": "9339116",
                "posx": 16,
                "posy": 26,
                "price": 0.0546,
                "reference": "R1",
                "referenceDisplacement": {
                    "x": 2.5,
                    "y": 0
                },
                "shape": "R-04",
                "symbol": "resistor.cmp",
                "value": "120",
                "valueDisplacement": {
                    "x": 2.5,
                    "y": 1
                }
            },
            {
                "angle": 0,
                "mirrored": false,
                "ordercode": "9339116",
                "posx": 11,
                "posy": 26,
                "price": 0.0546,
                "reference": "R2",
                "referenceDisplacement": {
                    "x": 2.5,
                    "y": 0
                },
                "shape": "R-04",
                "symbol": "resistor.cmp",
                "value": "120",
                "valueDisplacement": {
                    "x": 2.5,
                    "y": 1
                }
            },
            {
                "angle": 0,
                "mirrored": false,
                "ordercode": "1105982",
                "posx": 21,
                "posy": 54,
                "price": 0.691,
                "reference": "Q4",
                "referenceDisplacement": {
                    "x": 0,
                    "y": -1
                },
                "shape": "DIL-16",
                "symbol": "74165.cmp",
                "value": "74HCT165",
                "valueDisplacement": {
                    "x": 0,
                    "y": 9
                }
            },
            {
                "angle": 0,
                "mirrored": true,
                "ordercode": "1087086",
                "posx": 74,
                "posy": 11,
                "price": 0.294,
                "reference": "Q5",
                "referenceDisplacement": {
                    "x": 0,
                    "y": -2
                },
                "shape": "TO-220",
                "symbol": "regulator.cmp",
                "value": "7805",
                "valueDisplacement": {
                    "x": 0,
                    "y": -1
                }
            },
            {
                "angle": 0,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 82,
                "posy": 15,
                "price": 0.0735,
                "reference": "C3",
                "referenceDisplacement": {
                    "x": 2.5,
                    "y": 0
                },
                "shape": "C-02",
                "symbol": "capacitor.cmp",
                "value": "100n",
                "valueDisplacement": {
                    "x": 2.5,
                    "y": 1
                }
            },
            {
                "angle": 0,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 69,
                "posy": 15,
                "price": 0.0735,
                "reference": "C4",
                "referenceDisplacement": {
                    "x": 2.5,
                    "y": 0
                },
                "shape": "C-02",
                "symbol": "capacitor.cmp",
                "value": "100n",
                "valueDisplacement": {
                    "x": 2.5,
                    "y": 1
                }
            },
            {
                "angle": 0,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 52,
                "posy": 67,
                "price": 0.0735,
                "reference": "C5",
                "referenceDisplacement": {
                    "x": 2.5,
                    "y": 0
                },
                "shape": "C-02",
                "symbol": "capacitor.cmp",
                "value": "100n",
                "valueDisplacement": {
                    "x": 2.5,
                    "y": 1
                }
            },
            {
                "angle": 0,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 58,
                "posy": 67,
                "price": 0.0735,
                "reference": "C6",
                "referenceDisplacement": {
                    "x": 2.5,
                    "y": 0
                },
                "shape": "C-02",
                "symbol": "capacitor.cmp",
                "value": "100n",
                "valueDisplacement": {
                    "x": 2.5,
                    "y": 1
                }
            },
            {
                "angle": 0,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 64,
                "posy": 67,
                "price": 0.0735,
                "reference": "C7",
                "referenceDisplacement": {
                    "x": 2.5,
                    "y": 0
                },
                "shape": "C-02",
                "symbol": "capacitor.cmp",
                "value": "100n",
                "valueDisplacement": {
                    "x": 2.5,
                    "y": 1
                }
            },
            {
                "angle": 0,
                "mirrored": false,
                "ordercode": "1022230",
                "posx": 50,
                "posy": 18,
                "price": 0.139,
                "reference": "K2",
                "referenceDisplacement": {
                    "x": 0,
                    "y": -1
                },
                "shape": "SIL-2X03",
                "symbol": "header2x3.sbl",
                "value": "ICE",
                "valueDisplacement": {
                    "x": 0,
                    "y": 4
                }
            },
            {
                "angle": 0,
                "mirrored": false,
                "ordercode": "9339353",
                "posx": 61,
                "posy": 25,
                "price": 0.0529,
                "reference": "R4",
                "referenceDisplacement": {
                    "x": 2.5,
                    "y": 0
                },
                "shape": "R-04",
                "symbol": "resistor.sbl",
                "value": "270",
                "valueDisplacement": {
                    "x": 2.5,
                    "y": 1
                }
            },
            {
                "angle": 90,
                "mirrored": false,
                "ordercode": "2112100",
                "posx": 61,
                "posy": 15,
                "price": 0.231,
                "reference": "D1",
                "referenceDisplacement": {
                    "x": 2.5,
                    "y": 0
                },
                "shape": "LED-3MM",
                "symbol": "led.sbl",
                "value": "LED",
                "valueDisplacement": {
                    "x": 2.5,
                    "y": 1
                }
            },
            {
                "angle": 270,
                "mirrored": false,
                "ordercode": "9471596",
                "posx": 9,
                "posy": 65,
                "price": 1.31,
                "reference": "S1",
                "referenceDisplacement": {
                    "x": 9.5,
                    "y": 0
                },
                "shape": "DIL-16",
                "symbol": "dipswitch8.sbl",
                "value": "ADDRESS",
                "valueDisplacement": {
                    "x": 9.5,
                    "y": 1
                }
            },
            {
                "angle": 270,
                "mirrored": true,
                "ordercode": "9356819",
                "posx": 9,
                "posy": 45,
                "price": 0.55,
                "reference": "R3",
                "referenceDisplacement": {
                    "x": 10.5,
                    "y": 0
                },
                "shape": "SIL-1X09",
                "symbol": "rnet.sbl",
                "value": "8x10k",
                "valueDisplacement": {
                    "x": 10.5,
                    "y": 1
                }
            }
        ],
        "junction": [
            {
                "angle": 0,
                "mirrored": false,
                "posx": 30,
                "posy": 36
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 36,
                "posy": 36
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 32,
                "posy": 22
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 19,
                "posy": 22
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 20,
                "posy": 21
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 57,
                "posy": 32
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 31,
                "posy": 60
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 11,
                "posy": 73
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 12,
                "posy": 73
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 13,
                "posy": 73
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 14,
                "posy": 73
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 15,
                "posy": 73
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 16,
                "posy": 73
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 17,
                "posy": 73
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 32,
                "posy": 73
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 44,
                "posy": 73
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 32,
                "posy": 62
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 30,
                "posy": 43
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 36,
                "posy": 43
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 44,
                "posy": 43
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 57,
                "posy": 12
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 70,
                "posy": 12
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 83,
                "posy": 12
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 21,
                "posy": 20
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 17,
                "posy": 43
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 77,
                "posy": 22
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 83,
                "posy": 22
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 92,
                "posy": 37
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 53,
                "posy": 55
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 57,
                "posy": 55
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 59,
                "posy": 55
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 53,
                "posy": 73
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 59,
                "posy": 73
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 65,
                "posy": 73
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 10,
                "posy": 55
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 11,
                "posy": 56
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 12,
                "posy": 57
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 13,
                "posy": 58
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 14,
                "posy": 59
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 15,
                "posy": 60
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 16,
                "posy": 61
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 17,
                "posy": 62
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 92,
                "posy": 45
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 87,
                "posy": 36
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 22,
                "posy": 43
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 57,
                "posy": 19
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 70,
                "posy": 22
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 60,
                "posy": 35
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 59,
                "posy": 33
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 58,
                "posy": 34
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 62,
                "posy": 12
            }
        ],
        "sizex": 100,
        "sizey": 80,
        "text": [
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 79,
                "posy": 32,
                "rightAllign": false,
                "value": "A"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 79,
                "posy": 33,
                "rightAllign": false,
                "value": "B"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 79,
                "posy": 34,
                "rightAllign": false,
                "value": "C"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 79,
                "posy": 35,
                "rightAllign": false,
                "value": "D"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 79,
                "posy": 36,
                "rightAllign": false,
                "value": "E"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 86,
                "posy": 32,
                "rightAllign": false,
                "value": "F"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 86,
                "posy": 33,
                "rightAllign": false,
                "value": "G"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 86,
                "posy": 34,
                "rightAllign": false,
                "value": "P"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 86,
                "posy": 35,
                "rightAllign": false,
                "value": "+"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 86,
                "posy": 36,
                "rightAllign": false,
                "value": "-"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 4,
                "posy": 11,
                "rightAllign": false,
                "value": "B"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 4,
                "posy": 20,
                "rightAllign": false,
                "value": "B"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 4,
                "posy": 12,
                "rightAllign": false,
                "value": "A"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 4,
                "posy": 21,
                "rightAllign": false,
                "value": "A"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 4,
                "posy": 13,
                "rightAllign": false,
                "value": "C"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 4,
                "posy": 22,
                "rightAllign": false,
                "value": "C"
            }
        ],
        "wire": [
            {
                "points": [
                    {
                        "x": 71,
                        "y": 33
                    },
                    {
                        "x": 81,
                        "y": 33
                    },
                    {
                        "x": 81,
                        "y": 33
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 71,
                        "y": 34
                    },
                    {
                        "x": 81,
                        "y": 34
                    },
                    {
                        "x": 81,
                        "y": 34
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 71,
                        "y": 35
                    },
                    {
                        "x": 81,
                        "y": 35
                    },
                    {
                        "x": 81,
                        "y": 35
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 71,
                        "y": 36
                    },
                    {
                        "x": 81,
                        "y": 36
                    },
                    {
                        "x": 81,
                        "y": 36
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 71,
                        "y": 37
                    },
                    {
                        "x": 81,
                        "y": 37
                    },
                    {
                        "x": 81,
                        "y": 37
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 71,
                        "y": 38
                    },
                    {
                        "x": 80,
                        "y": 38
                    },
                    {
                        "x": 80,
                        "y": 40
                    },
                    {
                        "x": 88,
                        "y": 40
                    },
                    {
                        "x": 88,
                        "y": 35
                    },
                    {
                        "x": 85,
                        "y": 35
                    },
                    {
                        "x": 85,
                        "y": 35
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 72,
                        "y": 39
                    },
                    {
                        "x": 71,
                        "y": 39
                    },
                    {
                        "x": 71,
                        "y": 39
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 72,
                        "y": 39
                    },
                    {
                        "x": 79,
                        "y": 39
                    },
                    {
                        "x": 79,
                        "y": 41
                    },
                    {
                        "x": 89,
                        "y": 41
                    },
                    {
                        "x": 89,
                        "y": 41
                    },
                    {
                        "x": 89,
                        "y": 34
                    },
                    {
                        "x": 85,
                        "y": 34
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 71,
                        "y": 40
                    },
                    {
                        "x": 78,
                        "y": 40
                    },
                    {
                        "x": 78,
                        "y": 42
                    },
                    {
                        "x": 90,
                        "y": 42
                    },
                    {
                        "x": 90,
                        "y": 42
                    },
                    {
                        "x": 90,
                        "y": 33
                    },
                    {
                        "x": 85,
                        "y": 33
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 63,
                        "y": 34
                    },
                    {
                        "x": 56,
                        "y": 34
                    },
                    {
                        "x": 56,
                        "y": 34
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 56,
                        "y": 35
                    },
                    {
                        "x": 63,
                        "y": 35
                    },
                    {
                        "x": 63,
                        "y": 35
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 63,
                        "y": 36
                    },
                    {
                        "x": 56,
                        "y": 36
                    },
                    {
                        "x": 56,
                        "y": 36
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 56,
                        "y": 37
                    },
                    {
                        "x": 63,
                        "y": 37
                    },
                    {
                        "x": 63,
                        "y": 37
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 63,
                        "y": 38
                    },
                    {
                        "x": 56,
                        "y": 38
                    },
                    {
                        "x": 56,
                        "y": 38
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 56,
                        "y": 39
                    },
                    {
                        "x": 63,
                        "y": 39
                    },
                    {
                        "x": 63,
                        "y": 39
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 63,
                        "y": 40
                    },
                    {
                        "x": 56,
                        "y": 40
                    },
                    {
                        "x": 56,
                        "y": 40
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 35,
                        "y": 36
                    },
                    {
                        "x": 46,
                        "y": 36
                    },
                    {
                        "x": 46,
                        "y": 36
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 36,
                        "y": 38
                    },
                    {
                        "x": 36,
                        "y": 36
                    },
                    {
                        "x": 36,
                        "y": 36
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 46,
                        "y": 35
                    },
                    {
                        "x": 36,
                        "y": 35
                    },
                    {
                        "x": 36,
                        "y": 35
                    },
                    {
                        "x": 36,
                        "y": 33
                    },
                    {
                        "x": 30,
                        "y": 33
                    },
                    {
                        "x": 30,
                        "y": 38
                    },
                    {
                        "x": 30,
                        "y": 38
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 30,
                        "y": 36
                    },
                    {
                        "x": 31,
                        "y": 36
                    },
                    {
                        "x": 31,
                        "y": 36
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 56,
                        "y": 32
                    },
                    {
                        "x": 57,
                        "y": 32
                    },
                    {
                        "x": 57,
                        "y": 15
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 46,
                        "y": 41
                    },
                    {
                        "x": 44,
                        "y": 41
                    },
                    {
                        "x": 44,
                        "y": 41
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 31,
                        "y": 20
                    },
                    {
                        "x": 42,
                        "y": 20
                    },
                    {
                        "x": 42,
                        "y": 33
                    },
                    {
                        "x": 46,
                        "y": 33
                    },
                    {
                        "x": 46,
                        "y": 33
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 31,
                        "y": 22
                    },
                    {
                        "x": 41,
                        "y": 22
                    },
                    {
                        "x": 41,
                        "y": 37
                    },
                    {
                        "x": 46,
                        "y": 37
                    },
                    {
                        "x": 46,
                        "y": 37
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 31,
                        "y": 21
                    },
                    {
                        "x": 32,
                        "y": 21
                    },
                    {
                        "x": 32,
                        "y": 22
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 31,
                        "y": 23
                    },
                    {
                        "x": 40,
                        "y": 23
                    },
                    {
                        "x": 40,
                        "y": 34
                    },
                    {
                        "x": 46,
                        "y": 34
                    },
                    {
                        "x": 46,
                        "y": 34
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 10,
                        "y": 21
                    },
                    {
                        "x": 23,
                        "y": 21
                    },
                    {
                        "x": 23,
                        "y": 21
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 10,
                        "y": 22
                    },
                    {
                        "x": 23,
                        "y": 22
                    },
                    {
                        "x": 23,
                        "y": 22
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 10,
                        "y": 12
                    },
                    {
                        "x": 20,
                        "y": 12
                    },
                    {
                        "x": 20,
                        "y": 21
                    },
                    {
                        "x": 20,
                        "y": 21
                    },
                    {
                        "x": 20,
                        "y": 21
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 10,
                        "y": 13
                    },
                    {
                        "x": 19,
                        "y": 13
                    },
                    {
                        "x": 19,
                        "y": 22
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 10,
                        "y": 14
                    },
                    {
                        "x": 17,
                        "y": 14
                    },
                    {
                        "x": 17,
                        "y": 25
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 10,
                        "y": 23
                    },
                    {
                        "x": 12,
                        "y": 23
                    },
                    {
                        "x": 12,
                        "y": 25
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 10,
                        "y": 51
                    },
                    {
                        "x": 10,
                        "y": 64
                    },
                    {
                        "x": 10,
                        "y": 64
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 11,
                        "y": 64
                    },
                    {
                        "x": 11,
                        "y": 51
                    },
                    {
                        "x": 11,
                        "y": 51
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 12,
                        "y": 51
                    },
                    {
                        "x": 12,
                        "y": 64
                    },
                    {
                        "x": 12,
                        "y": 64
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 13,
                        "y": 64
                    },
                    {
                        "x": 13,
                        "y": 51
                    },
                    {
                        "x": 13,
                        "y": 51
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 14,
                        "y": 51
                    },
                    {
                        "x": 14,
                        "y": 64
                    },
                    {
                        "x": 14,
                        "y": 64
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 15,
                        "y": 64
                    },
                    {
                        "x": 15,
                        "y": 51
                    },
                    {
                        "x": 15,
                        "y": 51
                    },
                    {
                        "x": 15,
                        "y": 51
                    },
                    {
                        "x": 15,
                        "y": 51
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 16,
                        "y": 51
                    },
                    {
                        "x": 16,
                        "y": 64
                    },
                    {
                        "x": 16,
                        "y": 64
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 17,
                        "y": 64
                    },
                    {
                        "x": 17,
                        "y": 51
                    },
                    {
                        "x": 17,
                        "y": 51
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 20,
                        "y": 55
                    },
                    {
                        "x": 10,
                        "y": 55
                    },
                    {
                        "x": 10,
                        "y": 55
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 11,
                        "y": 56
                    },
                    {
                        "x": 20,
                        "y": 56
                    },
                    {
                        "x": 20,
                        "y": 56
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 20,
                        "y": 57
                    },
                    {
                        "x": 12,
                        "y": 57
                    },
                    {
                        "x": 12,
                        "y": 57
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 13,
                        "y": 58
                    },
                    {
                        "x": 20,
                        "y": 58
                    },
                    {
                        "x": 20,
                        "y": 58
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 20,
                        "y": 59
                    },
                    {
                        "x": 14,
                        "y": 59
                    },
                    {
                        "x": 14,
                        "y": 59
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 15,
                        "y": 60
                    },
                    {
                        "x": 20,
                        "y": 60
                    },
                    {
                        "x": 20,
                        "y": 60
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 20,
                        "y": 61
                    },
                    {
                        "x": 16,
                        "y": 61
                    },
                    {
                        "x": 16,
                        "y": 61
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 17,
                        "y": 62
                    },
                    {
                        "x": 20,
                        "y": 62
                    },
                    {
                        "x": 20,
                        "y": 62
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 30,
                        "y": 55
                    },
                    {
                        "x": 57,
                        "y": 55
                    },
                    {
                        "x": 57,
                        "y": 32
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 30,
                        "y": 56
                    },
                    {
                        "x": 40,
                        "y": 56
                    },
                    {
                        "x": 40,
                        "y": 38
                    },
                    {
                        "x": 46,
                        "y": 38
                    },
                    {
                        "x": 46,
                        "y": 38
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 30,
                        "y": 58
                    },
                    {
                        "x": 41,
                        "y": 58
                    },
                    {
                        "x": 41,
                        "y": 39
                    },
                    {
                        "x": 46,
                        "y": 39
                    },
                    {
                        "x": 46,
                        "y": 39
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 30,
                        "y": 60
                    },
                    {
                        "x": 42,
                        "y": 60
                    },
                    {
                        "x": 42,
                        "y": 40
                    },
                    {
                        "x": 46,
                        "y": 40
                    },
                    {
                        "x": 46,
                        "y": 40
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 30,
                        "y": 59
                    },
                    {
                        "x": 31,
                        "y": 59
                    },
                    {
                        "x": 31,
                        "y": 60
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 10,
                        "y": 70
                    },
                    {
                        "x": 10,
                        "y": 73
                    },
                    {
                        "x": 92,
                        "y": 73
                    },
                    {
                        "x": 92,
                        "y": 37
                    },
                    {
                        "x": 85,
                        "y": 37
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 11,
                        "y": 70
                    },
                    {
                        "x": 11,
                        "y": 73
                    },
                    {
                        "x": 11,
                        "y": 73
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 12,
                        "y": 70
                    },
                    {
                        "x": 12,
                        "y": 73
                    },
                    {
                        "x": 12,
                        "y": 73
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 13,
                        "y": 70
                    },
                    {
                        "x": 13,
                        "y": 73
                    },
                    {
                        "x": 13,
                        "y": 73
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 14,
                        "y": 70
                    },
                    {
                        "x": 14,
                        "y": 73
                    },
                    {
                        "x": 14,
                        "y": 73
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 15,
                        "y": 70
                    },
                    {
                        "x": 15,
                        "y": 73
                    },
                    {
                        "x": 15,
                        "y": 73
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 16,
                        "y": 70
                    },
                    {
                        "x": 16,
                        "y": 73
                    },
                    {
                        "x": 16,
                        "y": 73
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 17,
                        "y": 70
                    },
                    {
                        "x": 17,
                        "y": 73
                    },
                    {
                        "x": 17,
                        "y": 73
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 30,
                        "y": 57
                    },
                    {
                        "x": 32,
                        "y": 57
                    },
                    {
                        "x": 32,
                        "y": 73
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 30,
                        "y": 62
                    },
                    {
                        "x": 32,
                        "y": 62
                    },
                    {
                        "x": 32,
                        "y": 62
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 30,
                        "y": 41
                    },
                    {
                        "x": 30,
                        "y": 43
                    },
                    {
                        "x": 44,
                        "y": 43
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 36,
                        "y": 41
                    },
                    {
                        "x": 36,
                        "y": 43
                    },
                    {
                        "x": 36,
                        "y": 43
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 23,
                        "y": 23
                    },
                    {
                        "x": 22,
                        "y": 23
                    },
                    {
                        "x": 22,
                        "y": 43
                    },
                    {
                        "x": 30,
                        "y": 43
                    },
                    {
                        "x": 30,
                        "y": 43
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 18,
                        "y": 51
                    },
                    {
                        "x": 21,
                        "y": 51
                    },
                    {
                        "x": 21,
                        "y": 20
                    },
                    {
                        "x": 23,
                        "y": 20
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 85,
                        "y": 36
                    },
                    {
                        "x": 87,
                        "y": 36
                    },
                    {
                        "x": 87,
                        "y": 12
                    },
                    {
                        "x": 81,
                        "y": 12
                    },
                    {
                        "x": 81,
                        "y": 12
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 83,
                        "y": 14
                    },
                    {
                        "x": 83,
                        "y": 12
                    },
                    {
                        "x": 83,
                        "y": 12
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 73,
                        "y": 12
                    },
                    {
                        "x": 21,
                        "y": 12
                    },
                    {
                        "x": 21,
                        "y": 20
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 57,
                        "y": 15
                    },
                    {
                        "x": 57,
                        "y": 12
                    },
                    {
                        "x": 57,
                        "y": 12
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 70,
                        "y": 14
                    },
                    {
                        "x": 70,
                        "y": 12
                    },
                    {
                        "x": 70,
                        "y": 12
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 12,
                        "y": 30
                    },
                    {
                        "x": 12,
                        "y": 43
                    },
                    {
                        "x": 22,
                        "y": 43
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 17,
                        "y": 30
                    },
                    {
                        "x": 17,
                        "y": 43
                    },
                    {
                        "x": 17,
                        "y": 43
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 70,
                        "y": 17
                    },
                    {
                        "x": 70,
                        "y": 22
                    },
                    {
                        "x": 92,
                        "y": 22
                    },
                    {
                        "x": 92,
                        "y": 37
                    },
                    {
                        "x": 92,
                        "y": 37
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 77,
                        "y": 17
                    },
                    {
                        "x": 77,
                        "y": 22
                    },
                    {
                        "x": 77,
                        "y": 22
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 83,
                        "y": 17
                    },
                    {
                        "x": 83,
                        "y": 22
                    },
                    {
                        "x": 83,
                        "y": 22
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 53,
                        "y": 69
                    },
                    {
                        "x": 53,
                        "y": 73
                    },
                    {
                        "x": 53,
                        "y": 73
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 59,
                        "y": 69
                    },
                    {
                        "x": 59,
                        "y": 73
                    },
                    {
                        "x": 59,
                        "y": 73
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 65,
                        "y": 69
                    },
                    {
                        "x": 65,
                        "y": 73
                    },
                    {
                        "x": 65,
                        "y": 73
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 65,
                        "y": 66
                    },
                    {
                        "x": 65,
                        "y": 55
                    },
                    {
                        "x": 57,
                        "y": 55
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 53,
                        "y": 66
                    },
                    {
                        "x": 53,
                        "y": 55
                    },
                    {
                        "x": 53,
                        "y": 55
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 59,
                        "y": 66
                    },
                    {
                        "x": 59,
                        "y": 55
                    },
                    {
                        "x": 59,
                        "y": 55
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 71,
                        "y": 41
                    },
                    {
                        "x": 77,
                        "y": 41
                    },
                    {
                        "x": 77,
                        "y": 43
                    },
                    {
                        "x": 87,
                        "y": 43
                    },
                    {
                        "x": 87,
                        "y": 36
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 63,
                        "y": 41
                    },
                    {
                        "x": 62,
                        "y": 41
                    },
                    {
                        "x": 62,
                        "y": 45
                    },
                    {
                        "x": 92,
                        "y": 45
                    },
                    {
                        "x": 92,
                        "y": 45
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 44,
                        "y": 73
                    },
                    {
                        "x": 44,
                        "y": 41
                    },
                    {
                        "x": 44,
                        "y": 41
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 53,
                        "y": 19
                    },
                    {
                        "x": 57,
                        "y": 19
                    },
                    {
                        "x": 57,
                        "y": 19
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 53,
                        "y": 21
                    },
                    {
                        "x": 54,
                        "y": 21
                    },
                    {
                        "x": 54,
                        "y": 22
                    },
                    {
                        "x": 70,
                        "y": 22
                    },
                    {
                        "x": 70,
                        "y": 22
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 49,
                        "y": 21
                    },
                    {
                        "x": 45,
                        "y": 21
                    },
                    {
                        "x": 45,
                        "y": 32
                    },
                    {
                        "x": 46,
                        "y": 32
                    },
                    {
                        "x": 46,
                        "y": 32
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 49,
                        "y": 19
                    },
                    {
                        "x": 46,
                        "y": 19
                    },
                    {
                        "x": 46,
                        "y": 28
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 56,
                        "y": 33
                    },
                    {
                        "x": 63,
                        "y": 33
                    },
                    {
                        "x": 63,
                        "y": 33
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 46,
                        "y": 28
                    },
                    {
                        "x": 58,
                        "y": 28
                    },
                    {
                        "x": 58,
                        "y": 34
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 59,
                        "y": 33
                    },
                    {
                        "x": 59,
                        "y": 27
                    },
                    {
                        "x": 47,
                        "y": 27
                    },
                    {
                        "x": 47,
                        "y": 20
                    },
                    {
                        "x": 49,
                        "y": 20
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 53,
                        "y": 20
                    },
                    {
                        "x": 56,
                        "y": 20
                    },
                    {
                        "x": 56,
                        "y": 26
                    },
                    {
                        "x": 60,
                        "y": 26
                    },
                    {
                        "x": 60,
                        "y": 35
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 62,
                        "y": 12
                    },
                    {
                        "x": 62,
                        "y": 14
                    },
                    {
                        "x": 62,
                        "y": 14
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 62,
                        "y": 29
                    },
                    {
                        "x": 62,
                        "y": 32
                    },
                    {
                        "x": 61,
                        "y": 32
                    },
                    {
                        "x": 61,
                        "y": 41
                    },
                    {
                        "x": 56,
                        "y": 41
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 62,
                        "y": 20
                    },
                    {
                        "x": 62,
                        "y": 24
                    },
                    {
                        "x": 62,
                        "y": 24
                    }
                ],
                "type": "wire"
            }
        ]
    }
}
