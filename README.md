**RS485 DISPLAY**

The purpose of this project is to provide an RS485 interface to a 7-segment display.
The project contains a PCB and firmware.

## PCB

The PCB design can be found in the directory *pcb*.
The schematic diagram is drawn with [QScheme](https://www.twisq.nl/site/qscheme/).
The PCB design is made with [LAYO1](http://baas.nl/layo1pcb/nl/index.html).

## Firmware

The firmware can be found in the directory *rs485display*.
It is written in assembly for the ATtiny2313 processor from Atmel (Microchip since 2017).
You can compile the firmare using AVR studio for example.

When programming, make sure that you'll program these FUSES:

1. Disable the CKDIV8 fuse;
2. Set the clock to External Crystal Oscillator 3.0 - 8.0 MHz;

## Usage

Each display should have a unique address. The address can be set by the DIP switches.
A switch in ON position is a 1, a switch in OFF position is a 0.
The text label on the PCB gives the value of each bit (84218421).
For example, for address 0x21 you'll need to set the switches to OFF - OFF - ON - OFF - OFF - OFF - OFF - ON.

The baudrate of the display is 115200 baud. The PCB contains no terminator resistor.
You can chain multiple displays using the two connectors.

You can put any data you like on the display with a simple command. In this form:
[01A]
In this example a frame is surrounded by square brackets.
A frame starts with the address in hexadecimal notation. In this example 01.
Use upper case characters for the address.

The character to print in this example is the letter A. You can print any ASCII character between 32 and 128.
If you want to light the decimal point, simply add a point. For example:
[01A.]

The display will not send anything back on the bus.
The LED will light during power up and also when a frame is received.
If the address is right, the LED will light longer that when the address is not right.

## Licence

This is OpenSource.

RS485DISPLAY is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RS485DISPLAY is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with qDAMEdit.  If not, see <http://www.gnu.org/licenses/>.

2018 TWISQ (https://www.twisq.nl)
